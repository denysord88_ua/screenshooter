package webserver;

import webserver.files.finder.FileProcessor;
import webserver.files.parser.FileParser;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;

/**
 * Created by Denys Ordynskiy on 21.05.2017.
 */
public class WebServer {
    public static int connectedClients = 0;
    public static final int MAX_CONNECTIONS = 50;
    private String port = null;
    private boolean alive = false;

    public WebServer (String aPort) {
        port = aPort;
    }

    public void start() throws Throwable {
        alive = true;
        ServerSocket ss = new ServerSocket(Integer.parseInt(port));
        while (alive) {
            Socket s = ss.accept();
            new Thread(new SocketProcessor(s)).start();
        }
    }

    private class SocketProcessor implements Runnable {
        private Socket s;
        private InputStream is;
        private OutputStream os;

        private SocketProcessor(Socket s) throws Throwable {
            this.s = s;
            this.is = s.getInputStream();
            this.os = s.getOutputStream();
        }

        public void run() {
            try {
                if(connectedClients >= MAX_CONNECTIONS) return;
                connectedClients++;
                LinkedHashMap<String, String> requestHeaders = readInputHeaders();
                String requestURL = FileParser.getTextByBoundaries("/", " HTTP", requestHeaders.get("method")).toLowerCase();
                if(requestURL.isEmpty()) requestURL = "index.html";

                LinkedHashMap<String, String> responseHTTPHeaders = fillResponseHTTPHeaders(requestHeaders, requestURL);
                if(requestURL.equals("?action=login")) {
                    if(requestHeaders.containsKey("post_data")) {
                        // todo: process post data
                    } else requestURL = "index.html";
                }

                if(requestURL.equals("getjson")) {
                    String response = "{\"email\":\"testMail\", \"password\": \"testPassword\"}";
                    responseHTTPHeaders.put("Content-Length: ", response.length() + "\r\n");
                    responseHTTPHeaders.put("Connection: ", "close\r\n\r\n");
                    responseHTTPHeaders.put("Content-Type: ", "application/json; charset=utf-8\r\n");
                    writeResponse(getBytesFromStringMap(responseHTTPHeaders), response.getBytes());
                }

                LinkedList<Path> siteFiles = FileProcessor.getFilesListWithSubdir(new File("www").toPath(), "*.*");

                boolean fileExist = false;
                for(Path file : siteFiles) {
                    if(file.toString().substring("www\\".length()).replaceAll("\\\\", "/").toLowerCase().equals(requestURL)) {
                        byte [] fileData = Files.readAllBytes(file);
                        responseHTTPHeaders.put("Content-Length: ", fileData.length + "\r\n");
                        responseHTTPHeaders.put("Connection: ", "close\r\n\r\n");
                        writeResponse(getBytesFromStringMap(responseHTTPHeaders), fileData);
                        fileExist = true;
                    }
                }
                if(!fileExist) {
                    String response = "Method " + requestURL + " not yet implemented";
                    responseHTTPHeaders.put("Content-Length: ", response.length() + "\r\n");
                    responseHTTPHeaders.put("Connection: ", "close\r\n\r\n");
                    writeResponse(getBytesFromStringMap(responseHTTPHeaders), response.getBytes());
                }
            } catch (Throwable t) {
                if (t.getMessage() != null) System.err.println(t.getMessage());
            } finally {
                connectedClients--;
                try {
                    s.close();
                } catch (Throwable t) {
                    if (t.getMessage() != null) System.err.println(t.getMessage());
                }
            }
        }

        private LinkedHashMap<String, String> readInputHeaders() throws Throwable {
            LinkedHashMap<String, String> request = new LinkedHashMap<>();
            request.put("Client-Address", s.getRemoteSocketAddress().toString());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            StringBuffer headersSB = new StringBuffer();
            boolean isPost = false;
            String toLog = "";
            toLog += "____________________________________" +
                    "\r\nDate - " + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")).format(new Date()) +
                    "\r\nClient - " + s.getRemoteSocketAddress().toString() +
                    "\r\n";
            while (true) {
                String s = br.readLine();
                toLog += s + "\r\n";
                if (s == null || s.isEmpty()) {
                    if (isPost) {
                        StringBuffer requestContent = new StringBuffer();
                        for (int i = 0; i < Integer.parseInt(request.get("Content-Length")); i++) {
                            requestContent.append((char) br.read());
                        }
                        request.put("post_data", requestContent.toString().replaceAll("\\r", "").replaceAll("\\n", ""));
                    }
                    break;
                }
                headersSB.append(s);
                String key = s.contains(":") && !s.toLowerCase().startsWith("get") && !s.toLowerCase().startsWith("post") ? s.substring(0, s.indexOf(":")) : "method";
                String val = s.contains(":") && !s.toLowerCase().startsWith("get") && !s.toLowerCase().startsWith("post") ? s.substring(s.indexOf(":") + 1).trim() : s.trim();
                request.put(key, val);
                if (s.toLowerCase().contains("content-length")) isPost = true;
            }
            if(request.containsKey("post_data")) toLog += "post_data:\r\n" + request.get("post_data") + "\r\n";
            toLog += "____________________________________";
            System.out.println(toLog);
            return request;
        }

        private void writeResponse(byte[] headers, byte[] body) throws Throwable {
            byte[] response = new byte[headers.length + body.length];
            System.arraycopy(headers, 0, response, 0, headers.length);
            System.arraycopy(body, 0, response, headers.length, body.length);
            os.write(response);
            os.flush();
        }

        private LinkedHashMap<String, String> fillResponseHTTPHeaders (LinkedHashMap <String, String> requestHTTPHeaders, String request) {
            LinkedHashMap<String, String> responseHTTPHeaders = new LinkedHashMap<>();
            responseHTTPHeaders.put("HTTP/1.1 2", "00 OK\r\n");
            responseHTTPHeaders.put("Server: ", "YarServer/2009-09-09\r\n");

            if(request.endsWith(".html") || request.endsWith(".htm")) {
                responseHTTPHeaders.put("Content-Type: ", "text/html; charset=utf-8\r\n");
            } else if(request.endsWith(".css")) {
                responseHTTPHeaders.put("Content-Type: ", "text/css,*/*; charset=utf-8\r\n");
            } else if(request.endsWith(".js")) {
                responseHTTPHeaders.put("Content-Type: ", "text/javascript; charset=utf-8\r\n");
            } else if(request.endsWith(".png")) {
                responseHTTPHeaders.put("Content-Type: ", "image/png\r\n");
            } else if(request.endsWith(".jpg")) {
                responseHTTPHeaders.put("Content-Type: ", "image/jpg\r\n");
            } else if(request.endsWith(".gif")) {
                responseHTTPHeaders.put("Content-Type: ", "image/gif\r\n");
            } else if(request.endsWith(".mp3")) {
                responseHTTPHeaders.put("Content-Type: ", "audio/mpeg\r\n");
            } else if(request.endsWith(".mp4")) {
                responseHTTPHeaders.put("Content-Type: ", "video/mp4\r\n");
            } else if(request.endsWith(".ico")) {
                responseHTTPHeaders.put("Content-Type: ", "image/vnd.microsoft.icon\r\n");
            } else {
                responseHTTPHeaders.put("Content-Type: ", "text/plain; charset=utf-8\r\n");
            }
            return responseHTTPHeaders;
        }
        private byte[] getBytesFromStringMap(LinkedHashMap<String, String> map) {
            StringBuffer sb = new StringBuffer();
            for (String key : map.keySet()) sb.append(key + map.get(key));
            return sb.toString().getBytes();
        }
    }
}
