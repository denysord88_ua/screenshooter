package webserver;

import javax.swing.*;

/**
 * Created by Denys Ordynskiy on 21.05.2017.
 */
public class Manage {
    public static void main (String [] args) {
        try {
            WebServer webServer = new WebServer(JOptionPane.showInputDialog("Set server port"));
            webServer.start();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
